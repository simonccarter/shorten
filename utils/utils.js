var LinkModel = require('../models/linkmodel.js').LinkModel;

var url_to_index = new Array();
var short_to_url = new Array();
var CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHUJKLMNOPQRSTUVWXYZ';
var id = 0;

exports.short_url_exists = function(link, cb){
	LinkModel.findOne({llink: link},function(err,doc){
		if(err){
			res.send(404);
		}else{
			res.send(doc);
		}
	});
}

exports.shorten_url = function(req,res,link){
	var link = link;
	LinkModel.count({}, function(err, count){
		if(err){
		}else{
			console.log("count: " + count);
			id = count;
			//we have the count, now double see if we have shortened this link before...
			LinkModel.findOne({llink: link},function(err,doc){
				if(err){
					res.send(503);
				}else{
					//if we find doc, return llink
					if(doc!=null){
							   res.send(201, doc)
					}else{
						shortern_url_local(id, link, function(err, llink, slink){
							var link = {
								slink : slink,		
								llink : llink 
							}
							var linkm = new LinkModel(link);
							linkm.save(function(err, data){
								if (err) {
								   return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
								}else {
								   res.json(data);
								}
								console.log("Sending 201");
								res.send(201, slink)
							});
						});
					}
				}
			});
		} 
	});
}

exports.get_long_link = function(link, cb){
        this.short_url_exists(link, function(err, llink){
                if(err){
			console.log("within get_long_link: no link found");
                        cb("true", null);
                }else{
			console.log("within get_long_link: " + llink);
                        cb(null, llink);
                }
        });
}

function shortern_url_local(id, link, cb){
        var short_url = num_to_base62(id);
        while (short_url.length < 5) { 
            short_url = CHARS[0] + short_url;
        }
        id++;
        cb (null, link, url_to_index[link]);
}

function num_to_base62(n) {
    if(n > 62) {
        return num_to_base62(Math.floor(n / 62)) + CHARS[n % 62];
    } else {
        return CHARS[n];
    }
}
