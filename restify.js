var restify = require('restify'),
	mongoose = require('mongoose'),
	Logger = require('bunyan'),
        api = require('./routes/api');

var log = new Logger.createLogger({
        name: 'restify-tutorial',
        serializers: {
            req: Logger.stdSerializers.req
        }
    });

//START SERVER
var server = restify.createServer({ name: 'mongo-api', log: log })
 server.listen(7000, function () {
 console.log('%s listening at %s', server.name, server.url)
})

server
 .use(restify.fullResponse())
 .use(restify.bodyParser())

server.pre(function (request, response, next) {
    request.log.info({ req: request }, 'REQUEST');
    next();
});

//create mongoose model
server.get('/link/:url', api.get); //get long url from short url
server.post('/link/:url', api.post); //shorten long ur

