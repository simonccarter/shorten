var utils = require('../utils/utils.js');
var util = require('util');

function get_long_link(link, cb){
	utils.short_url_exists(link, function(error, llink){
		if(error){
			cb("true", null);
		}else{
			cb(null, short_to_url[llink]);
		}
	});
}

exports.get = function(req, res, next){
	var link = req.params.link;
	console.log("in get api with link to find: " + link);
	utils.get_long_link(link, function(error, llink){
		if (error) {
			res.send(404);
		}else{
			var objToJson = {"link" : llink };
			res.send(JSON.stringify(objToJson));
		}
	});
}

exports.post = function(req, res){
	var link = req.params.link;
	console.log("in post api " + link);
	utils.shorten_url(req,res,link);
}


