var utils = require('../utils/utils.js');
var util = require('util');

exports.redirect = function(req, res){
	var llink = req.params.llink;
	console.log("www.redirect with short_url to resolve: " + llink);
	utils.get_long_link(llink, function(error, link){
		if(error){
			//log error
			console.log("could not find link");
		}else{
			console.log("link " + util.inspect(link));
			var s = link;
			console.log("var slink: " + s);
			res.redirect(s);
		}
	});	
}

