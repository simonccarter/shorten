var mongoose = require('mongoose');
var db = mongoose.createConnection('mongodb://localhost/urlshort');
console.log('Connected to mongodb');

var LinkSchema = mongoose.Schema({
  slink:{
    type: String,
  },
  llink:{
    type: String,
  },
  date: {
        type: Date,
        default: Date.now
  }
});

var LinkModel = db.model('links', LinkSchema);
module.exports  ={
        LinkModel : LinkModel
}

